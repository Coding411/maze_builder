package builder.maze;

public interface Maze {

    void buildChambers();

    void buildConnectors();

    void buildEntry();

    void buildExit();
}
