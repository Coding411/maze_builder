package builder.maze;

public class EnchantedMaze implements Maze {
    @Override
    public void buildChambers() {
        System.out.println("Built Castle rooms.");
    }

    @Override
    public void buildConnectors() {
        System.out.println("Built hallways.");
    }

    @Override
    public void buildEntry() {
        System.out.println("Built draw bridge gate.");
    }

    @Override
    public void buildExit() {
        System.out.println("Built treasure room.");
    }
}
