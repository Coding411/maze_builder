package builder.maze;

public class HobbitMaze implements Maze{
    @Override
    public void buildChambers() {
        System.out.println("Built caves.");
    }

    @Override
    public void buildConnectors() {
        System.out.println("Built tunnels.");
    }

    @Override
    public void buildEntry() {
        System.out.println("Built cave entry.");
    }

    @Override
    public void buildExit() {
        System.out.println("Built tree exit.");
    }
}
