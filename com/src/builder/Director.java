package builder;

import builder.maze.EnchantedMaze;
import builder.maze.HobbitMaze;
import builder.maze.Maze;

public class Director {

    // Non-parameterized build method. Builds
    public Maze buildHobbitMaze() {
        Maze maze = executeBuildSteps(new HobbitMaze());
        return maze;
    }

    // Non-parameterized build method. Builds
    public Maze buildMagicalMaze() {
        Maze maze = executeBuildSteps(new EnchantedMaze());
        return maze;
    }

    // Parameterized build method.
    public Maze buildHobbitMaze(final String mazeType) {
        if("magic".equalsIgnoreCase(mazeType)) {
            return buildMagicalMaze();
        } else {
            return buildHobbitMaze();
        }
    }

    private Maze executeBuildSteps(Maze maze) {
        maze.buildChambers();
        maze.buildConnectors();
        maze.buildEntry();
        maze.buildExit();
        return maze;
    }
}
