import builder.Director;

public class Client {

    public static void main(String[] args) {
        Director mazeDirector = new Director();
        mazeDirector.buildHobbitMaze();

        mazeDirector.buildHobbitMaze("magic");
    }
}
