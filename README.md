# MazeBuilder

## Definition
Separate the construction of a complex object from its representation so that the same construction process can create different representations.

## Applicability:
- When the algorithm for creating an object should be independent of the parts that make up the object and how they are assembled.

## Participants:
1) Builder: Interface for creating parts of a Product object. Builders use a step by step process to build the Product. Typically uses a model where results of construction requests can be appended to the product.
2) ConcreteBuilder: Constructs and assembles parts of the product by implementing the builder interface. Provides an interface for retrieving the object.
3) Director: Constructs an object using the Builder interface.
4) Product: Represents the object under construction. Products used by builder differ so greatly that no interface is needed for Product.

